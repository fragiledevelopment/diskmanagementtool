

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.model.Datentraeger;

/**
 * servlet for editing table 
 */
@WebServlet("/servlet_edit")
public class servlet_edit extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	servlet_table table = new servlet_table();
	
   
    public servlet_edit() {
        super();
        
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter output = response.getWriter();
		String editNummer = request.getParameter("editFachnummer");
		int editNumber = Integer.parseInt(editNummer);
		String editInterpret = request.getParameter("editInterpret");
		String editAlbum = request.getParameter("editAlbum");
		String indexGenreStr = request.getParameter("editGenre");
		int indexGenre = Integer.parseInt(indexGenreStr);
		String[] genreListe = {"Rock", "Pop", "RnB", "Metal", "Rap", "Klassik", "Sonstige"};
		String genre = genreListe[indexGenre];
		String editDatum = request.getParameter("editDatum");
		
		Datentraeger dt = new Datentraeger(editNumber, editInterpret, editAlbum, genre, editDatum);
		
		
		output.println("<html>" + table.head + 
				"<body>" + table.navbar );
		
		output.println("<div class=\"jumbotron\" style=\"margin-top: 5vh; margin-left: 20vw; margin-right:20vw\">\r\n" + 
				"  <h1 class=\"display-4\">Bearbeitung erfolgreich!</h1>\r\n" +
				"<hr>"
				);
		
		
		
		
		//TODO showing the edited element of table
		output.println(
				"<ul class=\"list-group list-group-horizontal-lg\">" + 
				"  <li class=\"list-group-item\">" + dt.getId() + "</li>" + 
				"  <li class=\"list-group-item\">" + dt.getInterpret() +"</li>" + 
				"  <li class=\"list-group-item\">" + dt.getAlbum() +"</li>"
				+ "<li class=\"list-group-item\">" + dt.getGenre() +"</li>"
				+ "<li class=\"list-group-item\">" + dt.getDatum() +"</li>" + 
				"</ul> "  
						);
		
		table.traeger.set(editNumber,dt);	
		
				
		output.println("</div>");			
		output.println(	
				table.bodyEnd
				+ "</html>");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

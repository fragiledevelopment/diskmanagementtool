package com.model;

import java.util.ArrayList;

public class Datentraeger {

		
		int id;

		String interpret, album, genre, datum;
		
		public Datentraeger(String interpret, String album, String genre, String datum) {
			super();
			this.interpret = interpret;
			this.album = album;
			this.genre = genre; 
			this.datum = datum;
		}
		
		public String getAlbum() {
			return album;
		}
		
		public void setAlbum(String album) {
			this.album = album;
		}
		
		public int getId() {
			return id;
		}
		
		public void setId(int id) {
			this.id = id;
		}
		
		public String getGenre() {
			return genre;
		}
		
		public void setGenre(String genre) {
			this.genre = genre;
		}
		
		public String getDatum() {
			return datum;
		}
		
		public void setDatum(String datum) {
			this.datum = datum;
		}
		
		public String getInterpret() {
			return interpret;
		}
		
		public void setInterpret(String interpret) {
			this.interpret = interpret;
		}
		
	}


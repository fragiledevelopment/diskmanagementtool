import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.function.UnaryOperator;

import com.model.Datentraeger;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;




/**
 * Servlet implementation class servlet_table
 */
@WebServlet("/servlet_table")
public class servlet_table extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	
	ArrayList<Datentraeger> traeger = new ArrayList<Datentraeger>();
	ArrayList<Integer> checkSpace = new ArrayList<Integer>(50);
	String head = 
			"<head>\r\n" + 
			"<meta charset=\"UTF-8\">\r\n" + 
			"<link rel=\"shortcut icon\" href=\"./pictures/table.png\" />" + 
			"<link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/icon?family=Material+Icons\">\r\n" + 
			"<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\" integrity=\"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh\" crossorigin=\"anonymous\">\r\n" + 
			"<script src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\" integrity=\"sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n\" crossorigin=\"anonymous\"></script>\r\n" + 
			"<script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js\" integrity=\"sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo\" crossorigin=\"anonymous\"></script>\r\n" + 
			"<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js\" integrity=\"sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6\" crossorigin=\"anonymous\"></script>\r\n" + 
			"<title>CD-Tool</title>\r\n" + 
			"</head>";
	
	String navbar =
			" <nav class=\"navbar navbar-expand-lg navbar-light bg-light border-bottom border-primary\">\r\n" + 
			"  <a class=\" text-white navbar-brand\" href=\"#\"><span class=\"badge badge-primary\"><img src=\"./pictures/table.png\" width=\"30vw\" height=\"30vh\"></span></a>" + 
			"  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n" + 
			"    <span class=\" navbar-toggler-icon\"></span>\r\n" + 
			"  </button>\r\n" +
		    "<ul class=\"navbar-nav mr-auto\">\r\n" + 
			"      <li class=\"nav-item active\">\r\n" + 
			"        <a class=\"t nav-link\" name=\"homeButton\" href=\"index.jsp\">Datenträger erstellen<span class=\"sr-only\">(current)</span></a>\r\n"
			+ 		"</li>" +
			"<li class=\"nav-item active\">"
			+ "<a class=\"nav-link\" href=\"table.jsp\">Excel Datei erstellen</a>" + 
			"</li>" +
			"  </ul>" + 
			"  </nav>";
	
	String thead = "<thead class=\"thead-dark\">"+
			"						<tr>" + 
			"			            <th scope=\\\"col\\\">ID</th>" + 
			"			            <th scope=\\\"col\\\">Interpret</th>" + 
			"			            <th scope=\\\"col\\\">Albumname</th>" + 
			"			            <th scope=\\\"col\\\">Genre</th>" + 
			"			            <th scope=\\\"col\\\">Kaufdatum</th>" + 
			"			            </tr>" + 
			"	    </thead>";
	
	String bodyStart =
			"<body>" + navbar +
		    "<br>" +
		    "<br>" +
			"<div class=\"jumbotron\" style=\"padding: 4vw 8vh; margin-top: 5vh; margin-left: 20vw; margin-right:20vw\">"
			+ "<h1>Tabelle von Datenträger</h1>"
			+ "<br>"
			+ "<br>" ; 

		
	String bodyEnd = "</table>" + 
			"    </div>" + 
			"</body>";

	private ServletRequest session;
	
	
	
    public servlet_table() {
        super();
        
    }

	
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Getting vars from forms in index.jsp 
		
		String interpret = request.getParameter("interpret");
		String album = request.getParameter("album");
		String fachnummer = request.getParameter("fachnummer");
		String datum = request.getParameter("datum");
		String[] genreListe = {"Rock", "Pop", "RnB", "Metal", "Rap", "Klassik", "Sonstige"};
		String genreString = request.getParameter("genre");
		int genreIndex = Integer.parseInt(genreString);
		String genre = genreListe[genreIndex];
		PrintWriter output = response.getWriter();
		int intFachnummer = 0;
		
		
		traeger.add(intFachnummer,new Datentraeger(interpret, album, genre, datum));
        
		intFachnummer++;
        
		
		output.println("<html>"
				+ head 
				+ bodyStart);
			
		//Table 
		output.println("<table class=\"table \">"
				+ thead
				);
		
		
	
		for(Datentraeger dt : traeger ) {
			output.println(
					 "<tr>"
					+ "<th scope=\"row\">" + traeger.indexOf(dt) + "</th>"
				    + "<td scope=\"row\">" + dt.getInterpret() + "</td>"
				    + "<td scope=\"row\">" + dt.getAlbum() + "</td>"
					+ "<td scope=\"row\">" + dt.getGenre() + "</td>"
					+ "<td scope=\"row\">" + dt.getDatum() + "</td>"
					+ "</tr>" 
					);
		} session.setAttribute("traeger", traeger);
		
		output.println("<tbody>"
				+ "</table>");
				
		output.println(	
				bodyEnd
				+ "</html>");
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	
	public ArrayList<Datentraeger> getTraeger() {
		return traeger;
	}


	public void setTraeger(ArrayList<Datentraeger> traeger) {
		this.traeger = traeger;
	}
}

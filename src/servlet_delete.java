
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/servlet_delete")
public class servlet_delete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public servlet_delete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// TODO Auto-generated method stub
		servlet_table table = new servlet_table();
		PrintWriter output  = response.getWriter();
		
		String loeschnummerString = request.getParameter("loeschnummer");
		int loeschnummer = Integer.parseInt(loeschnummerString);
		
		output.println(
			"<html>" + table.head + 
				"<body>" + table.navbar  + 
				"<div class=\"jumbotron bg-light\" style=\"padding: 4vw 8vh; margin-top: 5vh; margin-left: 20vw; margin-right:20vw\">" +  
				"<h1>Datentraeger Nr. "+ loeschnummerString +" wurde erfolgreich geloescht !</h1>" +
						"<br>"
						+ "<br>" +
				        "<form action=\"./servlet_showTable\">" +
						"<button type=\"submit\" name=\"movePage\" value=\"Save\" class=\"btn btn-primary\" >Zur Tabelle</button>" +
				        "</form>" +
						"</div>"
				        );
		
		output.println(table.bodyEnd + "</html>");
		table.traeger.remove(loeschnummer);
		
		
		if(request.getParameter("movePage") != null) {
			RequestDispatcher disp = request.getRequestDispatcher("/servlet_table");
			disp.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

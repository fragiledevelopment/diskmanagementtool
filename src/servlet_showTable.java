

import java.io.IOException;
import java.io.PrintWriter;

import com.model.Datentraeger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/servlet_showTable")
public class servlet_showTable extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public servlet_showTable() {
        super();
        
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    servlet_table Table = new servlet_table();
	    PrintWriter output = response.getWriter();
	    
	    
	    output.println("<html>"
				+ Table.head 
				+ Table.bodyStart);
	    
	    output.println("<table class=\"table \">"
				+ Table.thead
				);
		
		
	  if(Table.traeger.isEmpty()) {
		  output.println(
					 "<tr>"
					 + "<div class=\"alert alert-primary\" role=\"alert\">\r\n" + 
					 " Die Tabelle ist leer!   \r\n" + 
					 "</div>"
					+ "</tr>" 
					);
	  }
	  else {
		  for(Datentraeger dt : Table.traeger ) {
				output.println(
						 "<tr>"
						+ "<th scope=\"row\">" + Table.traeger.indexOf(dt) + "</th>"
					    + "<td scope=\"row\">" + dt.getInterpret() + "</td>"
					    + "<td scope=\"row\">" + dt.getAlbum() + "</td>"
						+ "<td scope=\"row\">" + dt.getGenre() + "</td>"
						+ "<td scope=\"row\">" + dt.getDatum() + "</td>"
						+ "</tr>" 
						);
			}
	  }
		
		
	    
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

<%@ page import="com.model.Datentraeger" %>
<%@page import="java.util.ArrayList" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="UTF-8"%>
<html >
<head>
<meta charset="UTF-8">
	<link rel="shortcut icon" href="./pictures/table.png" />
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script type="text/javascript" src="script.js"></script>
	<title>CD-Tool</title>
</head>

<body> 
  <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom border-primary">
  <a class=" text-white navbar-brand" href="#"><span class="badge badge-primary"><img src="./pictures/table.png" width="30vw" height="30vh"></span></a>
 	 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    	<span class="navbar-toggler-icon"></span>
  	</button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
  	<ul class="navbar-nav mr-auto">
      	 <li class="nav-item active">
       		<button type="button" class="btn" data-toggle="modal" data-target="#editModal">Datenträger bearbeiten</button>
      	 </li>
      	 <li>
      	 	<button type="button" class="btn" data-toggle="modal" data-target="#deleteModal" style="margin-left: 0.5vw">Datenträger löschen</button>
      	 </li>
      	 <li>      	 	
      	 	<button type="submit" id="showButton" class="btn" onClick="window.location.href='/CD-Verwaltung-JSP/servlet_showTable'">Tabelle ansehen</button>
      	 </li>
 	 </ul>
  </div>
	</nav>
 	<br>
 	
	<div class="jumbotron" aria-hidden="true" style="margin-top: 5vh; margin-left: 20vw; margin-right:20vw">
       <form action="./servlet_table" method="GET">
        	    	<div class="input-group flex-nowrap">
  			   				<input id="id_name" name="interpret" type="text" class="form-control" placeholder="Interpret" aria-describedby="addon-wrapping">
					</div>
					<br>
					<div class="input-group flex-nowrap">
  			   				<input id="id_name" name="album" type="text" class="form-control" placeholder="Albumname" aria-describedby="addon-wrapping">
					</div>
					<br>
					<select name="genre" class="custom-select" id="inputGroupSelect01">
    					<option	 value="0">Rock</option>
    					<option  value="1">Pop</option>
    					<option  value="2">RnB</option>
    					<option  value="3">Metal</option>
    					<option  value="4">Rap</option>
    					<option  value="5">Klassik</option>
    					<option  value="6">Sonstige</option>
 				    </select>
 				    <br>
 				    <br>
 				    
 				    <div class="input-group flex-nowrap">
 				    	<input type="date" class="form-control" name="datum" id="kaufdatum ">
 				    </div>
 				    
 				    <br>
 				  
 				    <div class="modal-footer">
        				<button type="button" class="btn btn-info" data-dismiss="modal">Abbrechen</button>
       		    		<input id="saveButton" name="save" class="btn btn-primary" type="submit" value="Speichern" onClick="setButtonVisible()" />				
                    </div> 
 		</form>   
</div>
					

<!-- Modal for editing Datenträger -->		

	<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 		<form action="./servlet_edit" method="GET" >
  			<div class="modal-dialog" role="document">
    			<div class="modal-content">
      				<div class="modal-header bg-primary">
        				<h5 class="modal-title text-white" id="exampleModalLabel">Datenträger bearbeiten</h5>
       	 					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          						<span aria-hidden="true">&times;</span>
       	 					</button>
      				</div>
      		<div class="modal-body">
      		
        <!-- Content of Modal -->	  
      			<div class="input-group flex-nowrap">
      		
  			   		<input id="id_fachnummer" name="editFachnummer" type="number" class="form-control" placeholder="Fachnummer" aria-describedby="addon-wrapping" min="0" max="50">
						</div>
						<br>
        	    		<div class="input-group flex-nowrap">
  			   				<input id="id_name" name="editInterpret" type="text" class="form-control" placeholder="Interpret" aria-describedby="addon-wrapping">
						</div>
							<br>
						<div class="input-group flex-nowrap">
  			   				<input id="id_name" name="editAlbum" type="text" class="form-control" placeholder="Albumname" aria-describedby="addon-wrapping">
						</div>
						<br>
						<select name="editGenre" class="custom-select" id="inputGroupSelect01" aria-hidden="true">
    						<option  value="0">Rock</option>
    						<option  value="1">Pop</option>
    						<option  value="2">RnB</option>
    						<option  value="3">Metal</option>
    						<option  value="4">Rap</option>
    						<option  value="5">Klassik</option>
    						<option  value="6">Sonstige</option>
 				    	</select>	
 				    	<br>
 				        <br>
 				    	<div class="input-group flex-nowrap">
 				    		<input type="date" class="form-control" name="editDatum" id="kaufdatum ">
 				    	</div>			   
      		</div>     		
      		
      <div class="modal-footer">
        	<button type="button" class="btn btn-info" data-dismiss="modal">Abbrechen</button>
        	<button type="submit" name="saveedit" class="btn btn-primary">Übernehmen</button>
      </div>
    	</div>
  			</div>
  				</form>
					</div>
                      <br>

<!-- Modal for deleting a Traeger -->
      		<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  					<form action="./servlet_delete" method="get" >
  					<div class="modal-dialog" role="document">
    					<div class="modal-content">
      						<div class="modal-header bg-primary">
       							 <h5 class="modal-title text-white">Datenträger löschen</h5>
        								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          										<span aria-hidden="true">&times;</span>
        								</button>
      						</div>
      							<div class="modal-body">
       								 <p>Geben Sie in das Feld die gewünschte Fachnummer ein, um den gewünschten Träger zu löschen.</p>
       								 <br>
       								 <div class="input-group flex-nowrap">
       								 	<input name="loeschnummer" class="form-control" min="0" max="50">
       								 </div>
      							</div>
      								<div class="modal-footer">
        									<button type="button" class="btn btn-info" data-dismiss="modal">Abbrechen</button>
        									<button type="submit" name="deleteButton" class="btn btn-primary" value="submit">Bestätigen</button>
      								</div>
   						 </div>
  					</div>
  					</form>
			</div>

</body>
</html>
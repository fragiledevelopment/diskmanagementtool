<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.model.Datentraeger"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="org.apache.jasper.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="#" />
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<title>CD-Tool</title>
</head>
<body>
<table class="table">
	<% 	
	    //Creates from this jsp-file a Excel-Report
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition","inline; filename=traeger.xls");

	%>
	<thead>
	    
		<tr>
			<th scope="row"> ID </th>
		    <th scope="row"> Interpret </th>
		    <th scope="row"> Album </th>
		    <th scope="row"> Genre </th>
		    <th scope="row"> Kaufdatum </th>
		</tr>
		
		<tr>
		<%
			servlet_table Table = new servlet_table();
		   	ArrayList<Datentraeger> traeger = Table.getTraeger();
		   	
			for(Datentraeger dt: traeger){
		%>
			<th scope="row"><% traeger.indexOf(dt); %></th>
			<th scope="row"><% dt.getInterpret(); %></th>
			<th scope="row"><% dt.getAlbum(); %></th>
			<th scope="row"><% dt.getGenre(); %></th>
			<th scope="row"><% dt.getDatum(); %></th>
		<%		
			}
		%>
		</tr>
		
		
	</thead>
	

  	<tbody>

	</tbody>
</table>
</body>
</html>